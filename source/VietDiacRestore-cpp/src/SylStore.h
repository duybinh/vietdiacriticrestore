#ifndef SYLMAP_H_
#define SYLMAP_H_

#include "configure.h"

#include <fstream>
#include <set>
#include <map>
#include <string>
#include <iostream>

namespace std {

class SylStore {
private:
	set<string> _sylSet; // set store all vietnamese syllable
	RestoreSylMap _sylMap;
	static SylStore* _instance;

public:
	SylStore();
	virtual ~SylStore();
	bool isVNESE(string syllabel);

	// get label of syllable (1: non diacritic, i+1 when i = index of syllable in map's value)
	int getLabel(const string &syl);

	// get original syllable for non-diacritic syllable by label
	string getSyllable(const string &sylNoDiac, int label);

	string typeOfSyl(string syl);

	static SylStore* instance();
};

} /* namespace std */
#endif /* SYLMAP_H_ */
