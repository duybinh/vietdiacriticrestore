/*
 * SylMap.cpp
 *
 *  Created on: 2012/09/19
 *      Author: anh
 */

#include "SylStore.h"
#include "TextProcessing.h"
#include <assert.h>

namespace std {

SylStore* SylStore::_instance = NULL;
SylStore* SylStore::instance() {
	if (_instance == NULL)
		_instance = new SylStore;
	return _instance;
}

SylStore::SylStore() {
	_sylSet.clear();
	_sylMap.clear();

	ifstream ifs(SYL_FILE.c_str());

	if (!ifs) {
		cout << "Failed to open file " << SYL_FILE << endl;
		return;
	}

	string syl;
	string sylNoDiac;
	while (ifs && getline(ifs, syl)) {
		syl = TextProcessing::toLower(syl);
		_sylSet.insert(syl);

		// store syllable and syllable removed diacritic to map
		sylNoDiac = TextProcessing::removeDiacritic(syl);
		RestoreSylMap::iterator it = _sylMap.find(sylNoDiac);
		if (it == _sylMap.end()) {
			set<string> s;
			s.insert(syl);
			_sylMap[sylNoDiac] = s;
		} else {
			it->second.insert(syl);
		}
	}
	ifs.close();
}

SylStore::~SylStore() {
	_sylSet.clear();
	_sylMap.clear();
}

bool SylStore::isVNESE(string syllabel) {
	syllabel = TextProcessing::toLower(syllabel);

	if (_sylMap.find(syllabel) != _sylMap.end())
		return true;

	if (_sylSet.find(syllabel) != _sylSet.end())
		return true;

	return false;
}

int SylStore::getLabel(const string &syllable) {
	string syl = TextProcessing::toLower(syllable);
	string sylNoDiac = TextProcessing::removeDiacritic(syl);
	if (sylNoDiac.compare(syl) == 0)
		return 1;

	RestoreSylMap::iterator it = _sylMap.find(sylNoDiac);
	if (it == _sylMap.end()) {
		return 1;
	}

	int index = 1;
	for (set<string>::iterator it1 = it->second.begin();
			it1 != it->second.end(); it1++) {

		index++;
		if (syl.compare(*it1) == 0)
			return index;
	}

	assert("ERROR: SylMap::getLabel");
	return 1;
}

string SylStore::getSyllable(const string &sylNoDiac, int label) {
	if (label <= 1)
		return sylNoDiac;

	RestoreSylMap::iterator it = _sylMap.find(
			TextProcessing::toLower(sylNoDiac));
	if (it == _sylMap.end()) {
		return sylNoDiac;
	}

	if (label > (int) it->second.size() + 1) {
		return sylNoDiac;
	}

	set<string>::iterator it1 = it->second.begin();
	std::advance(it1, label - 2);
	return *it1;
}

string SylStore::typeOfSyl(string syl) {
	bool VH = false;
	bool vt = false;
	bool cs = false;
	bool kh = false;

	for (int i = 0; i < int(syl.length()); ++i) {
		if (int(SYMBOLS.find(syl[i])) > -1)
			kh = true;
		if (syl[i] >= '0' && syl[i] <= '9')
			cs = true;
		if (syl[i] >= 'A' && syl[i] <= 'Z')
			VH = true;
		if (syl[i] >= 'a' && syl[i] <= 'z')
			vt = true;
	}

	/*
	 * O : other
	 * N : number
	 * U : Upper
	 * L : lower
	 */
	if (kh)
		return "O";
	if (cs && (!(VH || vt)))
		return "N";
	if (cs && (VH || vt))
		return "O";
	if (isVNESE(syl)) {
		if (VH)
			return "U";
		if (vt)
			return "L";
	}
	return "O";
}

} /* namespace std */
