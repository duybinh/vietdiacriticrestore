#include "Machine.h"
#include "TextProcessing.h"

#include "assert.h"
#include "sstream"

namespace std {

Machine::Machine(int windowLen, RunMode mode) {
	_winLen = windowLen;
	_runMode = mode;

	_model = NULL;
	_featStore = new FeatStore();
}

Machine::~Machine() {
	delete _featStore;
	delete _model;
}

void Machine::learn(string corpusPath) {
	ifstream ifs(corpusPath.c_str());
	if (!ifs) {
		cout << "Failed to open corpus file. " << endl;
		return;
	}

	cout << "Learning..." << endl;
	string sentence;
	size_t lineCount = 0;

	vector<SylObj>* vSyl = new vector<SylObj>();
	vector<FeatEntry*>* vFeat = new vector<FeatEntry*>();
	while (ifs && getline(ifs, sentence)) {
		vSyl->clear();

		extractSyl(sentence, vSyl);
		extractFeat(vSyl, vFeat);

		lineCount++;
		if (lineCount % 10000 == 0)
			cout << lineCount << endl;
	}
	cout << lineCount << " line, " << vFeat->size() << " entry" << endl;
	ifs.close();

	// get problem for training
	problem* prob = getProblem(vFeat);

	// free large memory
	clearFeats(vFeat);
	delete vFeat;
	vSyl->clear();
	delete vSyl;

	// run training
	training(prob);
	clearProblem(prob);
	delete prob;

	// select features and save result to file
	selectFeat();
}

string Machine::restoreDiac(string sentence) {
	vector<SylObj> vSyl;
	vector<FeatEntry*> vFeat;
	extractSyl(sentence, &vSyl);
	extractFeat(&vSyl, &vFeat);

	string result = "";
	string nonDiacSyl, syl;
	FeatEntry* ftEntry;
	for (size_t i = 0; i < vFeat.size(); ++i) {
		ftEntry = vFeat.at(i);
		feature_node* nodes = new feature_node[ftEntry->second->size() + 1];

		size_t j = 0;
		set<size_t>::iterator it = ftEntry->second->begin();
		for (; it != ftEntry->second->end(); ++it) {
			nodes[j].index = *it;
			nodes[j].value = 1;
			++j;
		}
		nodes[j].index = -1;

		double p = predict(_model, nodes);
		delete[] nodes;

		nonDiacSyl = vSyl.at(i + _winLen).syllabel;
		syl = SylStore::instance()->getSyllable(nonDiacSyl, p);
		result += syl + " ";
	}

	//delete problem and free memory
	clearFeats(&vFeat);
	vSyl.clear();

	return result;
}

// convert a integer(single char) to string
string Machine::itostr(int x) {
	ostringstream oss;
	oss << x;
	return oss.str();
}

// convert a feats format to liblinear's problem struct
problem* Machine::getProblem(vector<FeatEntry*>* feats) {

	size_t numOfFeat = feats->size();
	feature_node** x = new feature_node*[numOfFeat];
	double* y = new double[numOfFeat];

	for (size_t i = 0; i < numOfFeat; ++i) {
		y[i] = feats->at(i)->first;
		feature_node* xx = new feature_node[feats->at(i)->second->size() + 1];
		x[i] = xx;
		size_t j = 0;
		set<size_t>::iterator it = feats->at(i)->second->begin();
		for (; it != feats->at(i)->second->end(); ++it) {
			xx[j].index = *it;
			xx[j].value = 1;
			++j;
		}
		xx[j].index = -1;
	}

	problem* prob = new problem;
	prob->l = numOfFeat;
	prob->n = _featStore->size();
	prob->y = y;
	prob->x = x;
	prob->bias = -1;

	cout << " prob " << prob->l << "," << prob->n << endl;

	return prob;
}

void Machine::clearProblem(problem* prob) {
	for (int i = 0; i < prob->l; ++i)
		delete[] prob->x[i];
	delete[] prob->x;
	delete[] prob->y;
}

void Machine::clearFeats(vector<FeatEntry*>* feats) {
	vector<FeatEntry*>::iterator it = feats->begin();
	for (; it != feats->end(); ++it) {
		(*it)->second->clear();
		delete (*it)->second;
		delete *it;
	}
	feats->clear();
}

// Train problem by Liblinear L1R_LR
void Machine::training(problem* prob) {
	// init parameter
	parameter param;
	param.solver_type = L1R_LR; // type of Machine learning Algorithm
	param.eps = 0.01;
	param.C = 1;
	param.nr_weight = 0;
	param.weight_label = new int(1);
	param.weight = new double(1.0);

	clock_t start, finish;

	start = clock();

	cout << "Start training ..." << endl;
	_model = train(prob, &param);
	cout << "Finish training. " << endl;

	finish = clock();
	printf("training took %f seconds to execute\n",
			((double) (finish - start)) / CLOCKS_PER_SEC); //
}

void Machine::selectFeat() {
	// find unused feat index
	set<int> unusedFt;
	int featNum = _model->nr_feature;
	int labelNum = _model->nr_class;

	// find features that have weight is 0
	unusedFt.clear();
	for (int i = 0; i < featNum; i++) {
		bool ignore = true;
		for (int j = 0; j < labelNum; j++) {
			if (!isZero(_model->w[i * labelNum + j])) {
				ignore = false;
				break;
			}
		}
		if (ignore)
			unusedFt.insert(i + 1);
	}

	// remove unused feat in store and save to file
	_featStore->remove(&unusedFt);
	_featStore->save(MAP_FILE);

	// save model
	saveModel(&unusedFt);
}

void Machine::saveModel(set<int>* unusedFt) {
	int featNum = _featStore->size();
	int labelNum = _model->nr_class;

	// save model header
	FILE* fp = fopen(MODEL_FILE.c_str(), "w");
	fprintf(fp, "solver_type L1R_LR\n");
	fprintf(fp, "nr_class %d\n", labelNum);

	fprintf(fp, "label");
	for (int i = 0; i < labelNum; i++)
		fprintf(fp, " %d", _model->label[i]);
	fprintf(fp, "\n");

	fprintf(fp, "nr_feature %d\n", featNum);

	fprintf(fp, "bias %.16g\n", _model->bias);

	fprintf(fp, "w\n");

	// save model data
	for (int i = 0; i < _model->nr_feature; i++) {
		if (unusedFt->find(i + 1) == unusedFt->end()) {
			for (int j = 0; j < labelNum; j++) {
				fprintf(fp, "%.16g ", _model->w[i * labelNum + j]);
			}
			fprintf(fp, "\n");
		}
	}

	fclose(fp);
}

/* do close test */
//double Machine::close_test() {
//	int count = 0;
//	for (size_t i = 0; i < _feats->size(); ++i) {
//		if (predict(_model, _problem.x[i]) != _feats->get()->at(i)->first) {
//			count++;
//		}
//	}
//
//	double P = 100.0 - double(count) / double(_feats->size()) * 100.0;
//	cout << "Close test : " << P << "%" << endl;
//	cout << count << " : " << _feats->size() << endl;
//
//	return P;
//}
bool Machine::load() {
	cout << "Start loading ..." << endl;
	_model = load_model(MODEL_FILE.c_str());
	if (!_featStore->load(MAP_FILE)) {
		cout << "End loading ..." << endl;
		return false;
	}
	cout << "End loading ..." << endl;
	return true;
}

void Machine::extractSyl(string text, vector<SylObj>* sylObjs) {
	// normalize and add BOS to begin and end of sentence
	for (int i = 0; i < _winLen; ++i)
		text = "BOS " + text + " BOS";

	text = TextProcessing::normalize(text);

	// split to syllable
	vector<string> syllables = TextProcessing::split(text);
	SylObj sylObj;
	for (vector<string>::iterator it = syllables.begin(); it != syllables.end();
			it++) {

		if (_runMode == LEARN) {
			sylObj.label = SylStore::instance()->getLabel(*it);
			sylObj.syllabel = TextProcessing::removeDiacritic(*it);
		} else {
			sylObj.label = 1;
			sylObj.syllabel = *it;
		}
		sylObj.type = SylStore::instance()->typeOfSyl(sylObj.syllabel);
		sylObjs->push_back(sylObj);
	}
}

void Machine::extractFeat(vector<SylObj>* sylObjs,
		vector<FeatEntry*>* featEntries) {

	string meta, word, featStr;
	int featIndex;

	for (int curPos = _winLen; curPos <= (int) sylObjs->size() - _winLen - 1;
			++curPos) {
		set<size_t>* featSet = new set<size_t>;
		featSet->clear();

		// get 1-gram
		for (int gramPos = curPos - _winLen; gramPos <= curPos + _winLen;
				++gramPos) {

			meta = itostr(gramPos - curPos) + "|";

			// syllable gram
			featStr = meta + sylObjs->at(gramPos).syllabel;
			featIndex = _featStore->getNum(featStr, _runMode);
			featSet->insert(featIndex);

			// syllable type gram
			featStr = meta + sylObjs->at(gramPos).type;
			featIndex = _featStore->getNum(featStr, _runMode);
			featSet->insert(featIndex);
		}

		// get 2-gram
		for (int gramPos = curPos - _winLen; gramPos <= curPos + _winLen - 1;
				++gramPos) {

			meta = itostr(gramPos - curPos) + "||";

			// syllable gram
			featStr = meta + sylObjs->at(gramPos).syllabel + " "
					+ sylObjs->at(gramPos + 1).syllabel;
			featIndex = _featStore->getNum(featStr, _runMode);
			featSet->insert(featIndex);

			// syllable type gram
			featStr = meta + sylObjs->at(gramPos).type + " "
					+ sylObjs->at(gramPos + 1).type;
			featIndex = _featStore->getNum(featStr, _runMode);
			featSet->insert(featIndex);
		}

		// get dictionary features
		for (int wLen = 2; wLen <= MAX_WORD_LENGTH; ++wLen) {
			for (int wPos = curPos - wLen + 1; wPos <= curPos; ++wPos) {
				// build word
				word = sylObjs->at(wPos).syllabel;
				for (int i = wPos + 1; i <= wPos + wLen - 1; ++i)
					word += " " + sylObjs->at(i).syllabel;

				// check word in map
				if (WordStore::instance()->isRmDiacWord(word)) {
					meta = "D(" + itostr(wPos - curPos) + ")|";
					featStr = meta + word;
					featIndex = _featStore->getNum(meta + word, _runMode);
					featSet->insert(featIndex);
				}
			}
		}

		assert(featSet->size() > 0);

		featEntries->push_back(
				new FeatEntry(sylObjs->at(curPos).label, featSet));
	}
}

/* a double is 0 ? */
bool Machine::isZero(double x) {
	if (x < 1e-10 && x > -1e-10)
		return true;
	return false;
}

} /* namespace std */
