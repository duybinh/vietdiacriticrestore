#include "FeatStore.h"
#include "configure.h"

namespace std {

FeatStore::FeatStore() {
	_ftMap.clear();
}

FeatStore::~FeatStore() {
	_ftMap.clear();
}

size_t FeatStore::getNum(const string str, RunMode ref) {
	map<string, size_t>::iterator it = _ftMap.find(str);
	if (it == _ftMap.end()) {
		if (ref == LEARN) {
			size_t num = _ftMap.size() + 1;
			_ftMap.insert(pair<string, size_t>(str, num));
			return num;
		} else { // ref == PREDICT
			return _ftMap.size() + 2;
		}
	} else {
		return it->second;
	}
}

size_t FeatStore::size() {
	return _ftMap.size();
}

void FeatStore::save(string mapfile) {
	ofstream ofs(mapfile.c_str());

	for (map<string, size_t>::iterator it = _ftMap.begin(); it != _ftMap.end();
			++it) {
		ofs << it->first << " " << it->second << endl;
	}

	ofs.close();
	return;
}

void FeatStore::print() {
	for (map<string, size_t>::iterator it = _ftMap.begin(); it != _ftMap.end();
			++it) {
		cout << it->first << " " << it->second << endl;

	}
	return;
}

bool FeatStore::load(string path) {
	fstream ifs(path.c_str());
	if (!ifs) {
		printf("Failed to open %s", path.c_str());
		return false;
	}

	size_t value, pos;
	string buf;
	while (getline(ifs, buf)) {
		pos = buf.length() - 1;
		while (buf[pos] >= '0' && buf[pos] <= '9')
			--pos;
		value = 0;
		for (size_t i = pos + 1; i < buf.length(); ++i) {
			value = value * 10 + buf[i] - '0';
		}
		_ftMap.insert(pair<string, size_t>(buf.substr(0, pos), value));
	}

	ifs.close();
	return true;
}

void FeatStore::remove(set<int>* unused) {
	map<int, string> revertMap;
	for (map<string, size_t>::iterator it = _ftMap.begin(); it != _ftMap.end();
			++it) {
		revertMap[it->second] = it->first;
	}

	_ftMap.clear();

	int index = 1;
	for (map<int, string>::iterator it = revertMap.begin();
			it != revertMap.end(); it++) {
		if (unused->find(it->first) == unused->end()) {
			_ftMap.insert(pair<string, size_t>(it->second, index));
			index++;
		}
	}
}

} /* namespace std */
