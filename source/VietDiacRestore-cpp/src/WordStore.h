#ifndef DICMAP_H_
#define DICMAP_H_

#include "configure.h"
#include <map>
#include <string>
#include <fstream>
#include <iostream>

namespace std {

class WordStore {
private:
	set<string> _words; // vietnamese words
	set<string> _rmDiacWords; // vietnamese words removed diacritic
	static WordStore* _instance;

public:
	WordStore();
	virtual ~WordStore();

	bool isWord(const string str);
	bool isRmDiacWord(const string str);

	static WordStore* instance();
};

}  // namespace std

#endif /* DICMAP_H_ */
