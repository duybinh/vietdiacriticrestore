#include "WordStore.h"
#include "TextProcessing.h"

namespace std {

WordStore* WordStore::_instance = NULL;
WordStore* WordStore::instance() {
	if (_instance == NULL)
		_instance = new WordStore;
	return _instance;
}

WordStore::WordStore() {
	ifstream ifs(WORD_FILE.c_str());
	if (!ifs) {
		cout << "Failed to open file " << WORD_FILE << endl;
		return;
	}

	string str;
	while (ifs && getline(ifs, str)) {
		_words.insert(str);
		_rmDiacWords.insert(TextProcessing::removeDiacritic(str));
	}
	ifs.close();
}

WordStore::~WordStore() {
	_words.clear();
	_rmDiacWords.clear();
	return;
}

bool WordStore::isWord(const string str) {
	set<string>::iterator pos = _words.find(str);
	return (pos != _words.end());
}

bool WordStore::isRmDiacWord(const string str) {
	set<string>::iterator pos = _rmDiacWords.find(str);
	return (pos != _rmDiacWords.end());
}

}  // namespace std
