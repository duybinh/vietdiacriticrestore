#ifndef MACHINE_H_
#define MACHINE_H_

#include "configure.h"
#include "WordStore.h"
#include "FeatStore.h"
#include "SylStore.h"
#include "./liblinear/linear.h"

#include <string>
#include <time.h>
#include <ctime>

namespace std {

typedef struct SylObj {
	string syllabel;
	string type;
	int label;
} SylObj;

class Machine {
private:
	RunMode _runMode;
	FeatStore* _featStore;
	model* _model;
	int _winLen;

public:
	Machine(int window_length, RunMode ref);
	virtual ~Machine();

	// learn
	void learn(string corpusPath);

	// predictor
	bool load();
	string restoreDiac(string sentence);

private:
	double close_test();

	// sentence to syllables
	void extractSyl(string text, vector<SylObj>* output);

	// syllables to feature entries
	void extractFeat(vector<SylObj>* sylObjs, vector<FeatEntry*>* feats);

	// convert feature entries to problem for training
	problem* getProblem(vector<FeatEntry*>* feats);

	// train by liblinear
	void training(problem* prob);

	// removed unused features
	void selectFeat();
	void saveModel(set<int>* unusedFt);

	// utility methods
	void clearProblem(problem* prob);
	void clearFeats(vector<FeatEntry*>* feats);

	string itostr(int x);
	bool isZero(double x);

};

} /* namespace std */
#endif /* Machine_H_ */
