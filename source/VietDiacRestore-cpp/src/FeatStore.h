#ifndef STRMAP_H_
#define STRMAP_H_

#include "configure.h"

#include <map>
#include <string.h>
#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <iostream>

namespace std {

class FeatStore {
private:
	map<string, size_t> _ftMap;

public:
	FeatStore();
	virtual ~FeatStore();

	size_t getNum(const string str, RunMode ref);
	size_t size();

	void print();
	void save(string mapfile);
	bool load(string path);

	map<string, size_t>::iterator begin() {
		return _ftMap.begin();
	}

	map<string, size_t>::iterator end() {
		return _ftMap.end();
	}

	void remove(set<int>* unused);

};

} /* namespace std */
#endif /* STRMAP_H_ */
