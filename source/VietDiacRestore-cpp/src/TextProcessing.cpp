/*
 * TextProcessing.cpp
 *
 *  Created on: Apr 22, 2016
 *      Author: binhnd
 */

#include "TextProcessing.h"
#include <iostream>
#include <iostream>
#include <fstream>

namespace std {

map<string, set<string> > TextProcessing::MapDiacritic = _initMapDiacritic();

RestoreCharMap TextProcessing::_initMapDiacritic() {
	RestoreCharMap m;
	string tmp1[] = { "à", "ả", "ã", "á", "ạ", "ă", "ằ", "ẳ", "ẵ", "ắ", "ặ",
			"â", "ầ", "ẩ", "ẫ", "ấ", "ậ" };
	m["a"] = set<string>(tmp1, tmp1 + 17);

	string tmp2[] = { "è", "ẻ", "ẽ", "é", "ẹ", "ê", "ề", "ể", "ễ", "ế", "ệ" };
	m["e"] = set<string>(tmp2, tmp2 + 11);

	string tmp3[] = { "ì", "ỉ", "ĩ", "í", "ị" };
	m["i"] = set<string>(tmp3, tmp3 + 5);

	string tmp4[] = { "ò", "ỏ", "õ", "ó", "ọ", "ơ", "ờ", "ở", "ỡ", "ớ", "ợ",
			"ô", "ồ", "ổ", "ỗ", "ố", "ộ" };
	m["o"] = set<string>(tmp4, tmp4 + 17);

	string tmp5[] = { "ù", "ủ", "ũ", "ú", "ụ", "ư", "ừ", "ử", "ữ", "ứ", "ự" };
	m["u"] = set<string>(tmp5, tmp5 + 11);

	string tmp6[] = { "ỳ", "ỷ", "ỹ", "ý", "ỵ" };
	m["y"] = set<string>(tmp6, tmp6 + 5);

	string tmp7[] = { "đ" };
	m["d"] = set<string>(tmp7, tmp7 + 1);

	string tmp11[] = { "À", "Ả", "Ã", "Á", "Ạ", "Ă", "Ằ", "Ẳ", "Ẵ", "Ắ", "Ặ",
			"Â", "Ầ", "Ẩ", "Ẫ", "Ấ", "Ậ" };
	m["A"] = set<string>(tmp11, tmp11 + 17);

	string tmp12[] = { "È", "Ẻ", "Ẽ", "É", "Ẹ", "Ê", "Ề", "Ể", "Ễ", "Ế", "Ệ" };
	m["E"] = set<string>(tmp12, tmp12 + 11);

	string tmp13[] = { "Ì", "Ỉ", "Ĩ", "Í", "Ị" };
	m["I"] = set<string>(tmp13, tmp13 + 5);

	string tmp14[] = { "Ò", "Ỏ", "Õ", "Ó", "Ọ", "Ơ", "Ờ", "Ở", "Ỡ", "Ớ", "Ợ",
			"Ô", "Ồ", "Ổ", "Ỗ", "Ố", "Ộ" };
	m["O"] = set<string>(tmp14, tmp14 + 17);

	string tmp15[] = { "Ù", "Ủ", "Ũ", "Ú", "Ụ", "Ư", "Ừ", "Ử", "Ữ", "Ứ", "Ự" };
	m["U"] = set<string>(tmp15, tmp15 + 11);

	string tmp16[] = { "Ỳ", "Ỷ", "Ỹ", "Ý", "Ỵ" };
	m["Y"] = set<string>(tmp16, tmp16 + 5);

	string tmp17[] = { "Đ" };
	m["D"] = set<string>(tmp17, tmp17 + 1);

	return m;
}

string TextProcessing::removeDiacritic(const string input) {
	string tmp = input;
	size_t position;
	for (RestoreCharMap::iterator it = MapDiacritic.begin();
			it != MapDiacritic.end(); it++) {
		// replace diacritic character by non-diacritic character
		for (set<string>::iterator it1 = it->second.begin();
				it1 != it->second.end(); it1++) {
			while (true) {
				position = tmp.find(*it1);
				if (position == string::npos)
					break;
				tmp.replace(position, (*it1).size(), it->first);
			}
		}
	}
	return tmp;
}

bool TextProcessing::removeDiacritic(const string inputFile,
		const string outputFile) {

	ifstream ifs(inputFile.c_str());
	if (!ifs) {
		cout << "Failed to open " << inputFile << endl;
		return false;
	}

	ofstream ofs(outputFile.c_str());
	string buf;
	size_t number_lines = 0;
	while (getline(ifs, buf)) {
		ofs << removeDiacritic(buf) << endl;
		number_lines++;
	}

	ofs.close();
	ifs.close();

	cout << "Remove diacritic for " << number_lines << " line to file "
			<< outputFile << endl;

	return true;
}

vector<string> TextProcessing::split(const string input, const char splitter) {

	string buf;
	vector<string> result;

	for (int i = 0; i < (int) input.size(); i++) {
		if (input[i] == splitter) {
			if (buf.size() > 0) {
				result.push_back(buf);
				buf.clear();
			}
		} else {
			buf.append(1, input[i]);
		}
	}

	if (buf.size() > 0)
		result.push_back(buf);

	return result;
}

bool TextProcessing::subFile(const string inputFile, const string outputFile,
		int fromLine, int numLine) {

	ifstream ifs(inputFile.c_str());
	if (!ifs) {
		cout << "Failed to open " << inputFile << endl;
		return false;
	}

	ofstream ofs(outputFile.c_str());
	string buf;
	int number_lines = 0;
	while (getline(ifs, buf)) {
		if (number_lines >= fromLine) {
			if (numLine > 0 && number_lines > fromLine + numLine) {
				break;
			} else {
				ofs << buf << endl;
			}
		}
		number_lines++;
	}

	ofs.close();
	ifs.close();

	cout << "Copy completed to file " << outputFile << endl;

	return true;
}

string TextProcessing::_source[4] = { "…", "“", "”", "„" };
string TextProcessing::_replace[4] = { "...", "\"", "\"", "," };

string TextProcessing::normalize(string text) {
	// delete control characters, such as Return, tabs, ... and SPACE
	// in begin and end of string. Control characters's oct is smaller than 41.
	while (text[0] <= 40 && text.length() > 0)
		text.erase(0);

	size_t length = text.length();
	while (text[length - 1] <= 40 && length > 0)
		text.erase(--length);

	// replace some UTF-8 char by one byte char
	for (int i = 0; i < 4; ++i) {
		size_t position = text.find(_source[i]);
		while (position != string::npos) {
			text.replace(position, _source[i].length(), _replace[i]);
			position = text.find(_source[i], position + sizeof(_replace[i]));
		}
	}

	// segment symbols
	for (size_t i = 0; i < text.length(); ++i) {
		if (SYMBOLS.find(text[i]) != string::npos) {
			text.insert(i + 1, " ");
			text.insert(i, " ");
			i += 2;
		}
	}

	string output = "";
	// remove consecutive space and underscore
	for (size_t i = 0; i < text.length(); ++i) {
		if (text[i] != SPACE || text[i - 1] != SPACE) {
			output += text[i];
		}
	}

	return output;
}

bool TextProcessing::normalizeFile(const string inputFile,
		const string outputFile) {

	ifstream ifs(inputFile.c_str());
	if (!ifs) {
		cout << "Failed to open " << inputFile << endl;
		return false;
	}

	ofstream ofs(outputFile.c_str());
	string buf;
	int number_lines = 0;
	while (getline(ifs, buf)) {
		ofs << normalize(buf) << endl;
	}
	number_lines++;

	ofs.close();
	ifs.close();

	cout << "Normalize completed file " << inputFile << " to file "
			<< outputFile << endl;

	return true;
}
string TextProcessing::toLower(const string input) {
	string output;
	for (size_t i = 0; i < input.length(); i++) {
		output += tolower(input[i]);
	}
	return output;
}

} /* end of namespace */

