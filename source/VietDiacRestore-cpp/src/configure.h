#ifndef CONFIGURE_H_
#define CONFIGURE_H_

#include <string>
#include <set>
#include <map>
#include <vector>
#include <list>

namespace std {

typedef pair<size_t, set<size_t>*> FeatEntry;
typedef size_t RunMode;
typedef map<string, set<string> > RestoreSylMap; // non-diacritic syllable to list of syllable possible
typedef map<string, set<string> > RestoreCharMap; // non-diacritic character to set of diacritic character

const int MAX_WORD_LENGTH = 3;
const char SPACE = ' ';

const RunMode LEARN = 0;
const RunMode PREDICT = 1;
const string SYMBOLS = "@`#$%&~|[]<>'(){}*+-=;,?.!:\"/";

const string WORD_FILE = "data/wordlist.txt";
const string SYL_FILE = "data/VNsyl.txt";
const string MODEL_FILE = "model/model.txt";
const string MAP_FILE = "model/map.txt";



} /* end of namespace */
#endif /* CONFIGURE_H_ */
