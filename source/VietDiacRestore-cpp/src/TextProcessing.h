/*
 * TextProcessing.h
 *
 *  Created on: Apr 22, 2016
 *      Author: binhnd
 */

#ifndef TEXTPROCESSING_H_
#define TEXTPROCESSING_H_

#include "configure.h"
namespace std {

class TextProcessing {
public:
	static RestoreCharMap MapDiacritic;

	static string removeDiacritic(const string input);
	static bool removeDiacritic(const string inputFile,
			const string outputFile);
	static vector<string> split(const string input, const char splitter = ' ');
	static bool subFile(const string inputFile, const string outputFile,
			int fromLine = 0, int numLine = -1);

	static string normalize(string input);
	static bool normalizeFile(const string inputFile, const string outputFile);
	static string toLower(const string input);
private:
	static RestoreCharMap _initMapDiacritic();
	static string _source[4];
	static string _replace[4];

};
} /* end of namespace */

#endif /* TEXTPROCESSING_H_ */
