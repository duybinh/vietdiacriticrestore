//============================================================================
// Name        : pointwise_diacritic_restore.cpp
// Author      : binhnd
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <unistd.h>

#include "TextProcessing.h"
#include "Machine.h"

using namespace std;

int runPredictor(int argc, char **argv);
int runLeaner(int argc, char **argv);
int usage();

int main(int argc, char **argv) {
	// for testing

//	TextProcessing::subFile("corpus/VNTQcorpus(small).txt",
//			"corpus/corpus.txt", 100, 1000);
//	TextProcessing::subFile("corpus/VNTQcorpus(small).txt",
//			"test/test2-source.txt", 10100, 10);
//	TextProcessing::removeDiacritic("test/test3-source.txt", "test/test3.txt");
//	TextProcessing::normarlizeFile("corpus/corpus.txt", "corpus/corpus_n.txt");
//	string test = "Điền";
//	cout << TextProcessing::toLower(test) << endl;
//	return 0;

	if (argc == 5) {
		return runPredictor(argc, argv);
	}

	if (argc == 6) {
		return runLeaner(argc, argv);
	}

	return usage();

}

int usage() {
	cout << "./program -i input_file_path -o output_file_path" << endl;
	cout << "./program learn -i corpus_link -w window_length" << endl;
	return 1;
}

int runPredictor(int argc, char **argv) {

	extern char *optarg;
	int opt;

	string inputfile, outputfile;
	while ((opt = getopt(argc, argv, "i:o:")) != -1) {
		switch (opt) {
		case 'i':
			inputfile = optarg;
			break;
		case 'o':
			outputfile = optarg;
			break;
		default:
			break;
		}
	}

	//Predictor
	ifstream ifs1(inputfile.c_str());
	if (!ifs1) {
		cout << "Failed to open " << inputfile << endl;
		usage();
		return 1;
	}

	Machine predictor(3, PREDICT);
	if (!predictor.load()) {
		cout << "Failed to load model" << endl;
		return 1;
	}

	clock_t start = clock(), finish;
	cout << "Start segmenting ..." << endl;
	ofstream ofs(outputfile.c_str());
	string buf;
	int number_lines = 0;
	while (getline(ifs1, buf)) {
		ofs << predictor.restoreDiac(buf) << endl;
		number_lines++;
	}

	cout << "End segmenting." << endl;
	finish = clock();
	printf("Segment %d line(s). \n", number_lines);
	printf("Segmentation took %f seconds to execute\n",
			((double) (finish - start)) / CLOCKS_PER_SEC); //

	ofs.close();
	ifs1.close();

	return 0;
}

int runLeaner(int argc, char **argv) {

	extern char *optarg;
	int opt;
	string corpus_link;
	int WINDOW_LENGTH;
	while ((opt = getopt(argc, argv, "i:w:")) != -1) {
		switch (opt) {
		case 'i':
			corpus_link = optarg;
			break;

		case 'w':
			WINDOW_LENGTH = atoi(optarg);
			break;
		default:
			break;
		}
	}

	// Learner
	//cout << "Learning..." << endl;
	clock_t start = clock(), finish;

	Machine learner(WINDOW_LENGTH, LEARN);
	learner.learn(corpus_link);

	finish = clock();
	printf("Learning took %f seconds\n",
			((double) (finish - start)) / CLOCKS_PER_SEC); //

	return 0;
}

