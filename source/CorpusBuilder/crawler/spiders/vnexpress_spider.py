import scrapy
import re
import json

class VNExpressSpider(scrapy.Spider):
    name = "vnexpress"
    allowed_domains = ["vnexpress.net"]
    start_urls = [
        "http://vnexpress.net/tin-tuc/thoi-su/"        
    ]
    
    file_name = 'data.txt'
    page_count = 1
    
    
    def parse(self, response):	
	a_links = response.xpath('//div[@class="title_news"]/a[@class="txt_link"]/@href').extract()
	for link in a_links:
	    s = re.search(r'\-(\d+)\.html', link, re.M|re.I)
	    if s:
		a_id = s.group(1)		
		url = 'http://usi.saas.vnexpress.net/index/get?offset=0&limit=0&objectid='+a_id+'&objecttype=1&siteid=1000000'
		print url
		yield scrapy.Request(url, callback=self.parse_comments)

	VNExpressSpider.page_count += 1
	if VNExpressSpider.page_count <= 1000:
	    for url in VNExpressSpider.start_urls:
		url = url + "page/%d.html" % VNExpressSpider.page_count
		print url
		yield scrapy.Request(url, callback=self.parse)
          
    def parse_comments(self, response):
	j = json.loads(response.body_as_unicode())
	items = j['data']['items']
	for item in items:
	    content = item['content'].encode('utf-8')
	    content = self.normalize(content)
	    print content
	    with open(VNExpressSpider.file_name, 'a') as f:
		f.write(content + '\n')
    
    def normalize(self, s):
	s = re.sub(r'\<[^\>]*\>', ' ', s)
	s = re.sub(r'\&[^\;]*\;', ' ', s)
	return s

	    
	  

