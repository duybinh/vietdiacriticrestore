package vn.binh.diacrestore;

public class Program {
	static String wordPath = "data/wordlist.txt";
	static String sylPath = "data/VNsyl.txt";

	static String corpusPath = "corpus/corpus.txt";
	static String modelPath = "model/model.txt";
	static String featPath = "model/ft.txt";

	static void usage() {
		System.out.println("Usage:");
		System.out.println("... learn corpus_path");
		System.out.println("... predict input_path output_path");
		System.out.println("... predict sentence");
		System.out.println("... rm_diac input_path output_path");
		System.out.println("... normalize input_path output_path");
		System.out.println("... copy input_path output_path from_line to_line");
	}

	public static void main(String[] args) {
		try {
			if (args.length < 2)
				throw new IllegalArgumentException();

			String mode = args[0];
			if (mode.equals("learn")) {
				corpusPath = args[1];
				runLearner();
			} else if (mode.equals("predict")) {
				if (args.length == 2) {
					runPredictorString(args[1]);
				} else if (args.length == 3) {
					runPredictorFile(args[1], args[2]);
				} else {
					throw new IllegalArgumentException();
				}

			} else if (mode.equals("rm_diac")) {
				if (args.length == 3) {
					TextProcessing.removeDiacFile(args[1], args[2]);
				} else {
					throw new IllegalArgumentException();
				}

			} else if (mode.equals("normalize")) {
				if (args.length == 3) {
					Global.sylStore = new SylStore(sylPath);
					TextProcessing.normalizeFile(args[1], args[2]);
				} else {
					throw new IllegalArgumentException();
				}

			} else if (mode.equals("copy")) {
				if (args.length == 5) {
					TextProcessing.splitFile(args[1], args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]));
				} else {
					throw new IllegalArgumentException();
				}

			} else {
				throw new IllegalArgumentException();
			}
		} catch (Exception e) {
			usage();
		}
	}

	static void runLearner() {
		Global.wordStore = new WordStore(wordPath);
		Global.sylStore = new SylStore(sylPath);

		Learner learner = new Learner(corpusPath, modelPath, featPath);
		learner.train(true);
	}

	static void runPredictorFile(String inputPath, String outputPath) {
		Global.wordStore = new WordStore(wordPath);
		Global.sylStore = new SylStore(sylPath);

		Predictor predictor = new Predictor(modelPath, featPath);
		predictor.restoreDiac(inputPath, outputPath);
	}

	static void runPredictorString(String input) {
		Global.wordStore = new WordStore(wordPath);
		Global.sylStore = new SylStore(sylPath);

		Predictor predictor = new Predictor(modelPath, featPath);
		String output = predictor.restoreDiac(input);
		System.out.println(output);
	}

}
