package vn.binh.diacrestore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Class for storing all features and manage these indexes
 * 
 * @author binhnd
 * 
 */
public class FeatStore {
	private Map<String, Integer> featMap;
	private String filePath;

	public FeatStore(String _filePath) {
		filePath = _filePath;
		featMap = new HashMap<String, Integer>();
	}

	/**
	 * insert feature to map if not exist
	 * 
	 * @param feat
	 * @return index of this feature
	 */
	public int insertFeat(String feat) {
		if (featMap.containsKey(feat)) {
			return featMap.get(feat);
		}

		int value = featMap.size() + 1;
		featMap.put(feat, value);
		return value;
	}

	/**
	 * Get the index of a feature
	 * 
	 * @param feat
	 * @return index of feature if exist, (max size + 2) if not exist
	 */
	public int getFeatIndex(String feat) {
		if (featMap.containsKey(feat)) {
			return featMap.get(feat);
		}

		return featMap.size() + 2;
	}

	/**
	 * remove all unused features by index
	 * 
	 * @param unusedIndex
	 */
	public void removeFeats(Set<Integer> unusedIndex) {
		SortedMap<Integer, String> sortedFeatMap = new TreeMap<>();
		for (Entry<String, Integer> entry : featMap.entrySet()) {
			if (!unusedIndex.contains(entry.getValue())) {
				sortedFeatMap.put(entry.getValue(), entry.getKey());
			}
		}

		featMap.clear();

		int index = 1;
		for (Entry<Integer, String> entry : sortedFeatMap.entrySet()) {
			featMap.put(entry.getValue(), index++);
		}

		Logger.log("Remove " + unusedIndex.size() + " features");
	}

	/**
	 * load data from file if exist
	 */
	public void load() {
		try {
			BufferedReader bufReader = new BufferedReader(new FileReader(filePath));
			String line;
			while ((line = bufReader.readLine()) != null) {
				int i = line.lastIndexOf(" ");
				if (i != -1) {
					String feat = line.substring(0, i);
					int index = Integer.parseInt(line.substring(i + 1));
					featMap.put(feat, index);
				}
			}
			bufReader.close();

			Logger.log("Loaded total features: " + featMap.size());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * save data to file
	 */
	public void save() {
		try {
			FileWriter fw = new FileWriter(filePath);

			for (Map.Entry<String, Integer> entry : featMap.entrySet()) {
				fw.write(entry.getKey() + " " + entry.getValue() + "\n");
			}
			fw.flush();
			fw.close();

			Logger.log("Write total features: " + featMap.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int size() {
		return featMap.size();
	}
}
