package vn.binh.diacrestore;

public class Logger {
	public enum Level {
		Error, Warn, Info, Debug
	}

	public static void log(String msg, Level level) {
		System.out.println(msg);
	}

	public static void log(String msg) {
		log(msg, Level.Info);
	}
}
