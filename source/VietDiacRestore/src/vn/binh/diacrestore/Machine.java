package vn.binh.diacrestore;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import vn.binh.diacrestore.SylStore.SylType;
import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Model;

/**
 * Abstract machine for learning or prediction
 * 
 * @author binhnd
 * 
 */
public abstract class Machine {

	// Class contain information of a syllable
	protected class SylObj {
		private String sylNoDiac;
		private String syl;
		private SylType type;
		private int label;

		public String getSyl() {
			return syl;
		}

		public void setSyl(String _syl) {
			this.syl = _syl;
		}

		public SylType getType() {
			return type;
		}

		public void setType(SylType _type) {
			this.type = _type;
		}

		public int getLabel() {
			return label;
		}

		public void setLabel(int _label) {
			this.label = _label;
		}

		public String getSylNoDiac() {
			return sylNoDiac;
		}

		public void setSylNoDiac(String _sylNoDiac) {
			this.sylNoDiac = _sylNoDiac;
		}

	}

	// Class contain information of a training entry
	protected class FeatEntry extends SylObj {
		private SortedSet<Integer> ftIndexSet;

		public FeatEntry() {
			setLabel(Global.DEFAULT_LABEL);
			ftIndexSet = new TreeSet<>();
		}

		public Set<Integer> getFtIndexSet() {
			return ftIndexSet;
		}

		public Feature[] getFtArr() {
			Feature[] lFt = new Feature[ftIndexSet.size()];
			int col = 0;
			for (Integer ft : ftIndexSet) {
				lFt[col] = new FeatureNode(ft, 1);
				col++;
			}
			return lFt;
		}
	}

	protected Model model;
	protected FeatStore featStore;
	protected String modelPath;
	protected String featPath;

	public Machine(String _modelPath, String _featPath) {
		modelPath = _modelPath;
		featPath = _featPath;
		featStore = new FeatStore(featPath);
	}

	/**
	 * Extract a sentence to list of syllables
	 * 
	 * @param sentence
	 * @return
	 */
	protected abstract List<FeatEntry> extractSyl(String sentence);

	protected abstract int getFeatIndex(String feat);

	/**
	 * Extract a sentence to list of feature entry
	 * 
	 * @param sentence
	 * @return
	 */
	protected List<FeatEntry> extractFeat(String sentence) {
		List<FeatEntry> lEntry = extractSyl(sentence);

		int numEntry = lEntry.size();
		String featStr;
		int featIndex;
		FeatEntry curEntry;

		for (int entryPos = Global.WINDOW_LEN; entryPos <= numEntry - 1 - Global.WINDOW_LEN; entryPos++) {
			curEntry = lEntry.get(entryPos);

			// 1 gram feature
			for (int gramPos = entryPos - Global.WINDOW_LEN; gramPos <= entryPos + Global.WINDOW_LEN; gramPos++) {
				featStr = (gramPos - entryPos) + "|" + lEntry.get(gramPos).getSylNoDiac();
				featIndex = getFeatIndex(featStr);
				curEntry.getFtIndexSet().add(featIndex);

				featStr = (gramPos - entryPos) + "|" + lEntry.get(gramPos).getType();
				featIndex = getFeatIndex(featStr);
				curEntry.getFtIndexSet().add(featIndex);
			}

			// 2 gram feature
			for (int gramPos = entryPos - Global.WINDOW_LEN; gramPos <= entryPos + Global.WINDOW_LEN - 1; gramPos++) {

				featStr = (gramPos - entryPos) + "||" + lEntry.get(gramPos).getSylNoDiac() + " " + lEntry.get(gramPos + 1).getSylNoDiac();
				featIndex = getFeatIndex(featStr);
				curEntry.getFtIndexSet().add(featIndex);

				featStr = (gramPos - entryPos) + "||" + lEntry.get(gramPos).getType() + lEntry.get(gramPos + 1).getType();
				featIndex = getFeatIndex(featStr);
				curEntry.getFtIndexSet().add(featIndex);
			}

			// dictionary feature
			for (int wLen = 2; wLen <= Global.MAX_WORD_LEN; wLen++) {
				for (int gramPos = entryPos - wLen + 1; gramPos <= entryPos; gramPos++) {
					String word = "";
					for (int i = 0; i < wLen; i++) {
						word += lEntry.get(gramPos + i).getSylNoDiac() + " ";
					}
					word = word.trim();
					if (Global.wordStore.isWord(word, true)) {
						featStr = "D(" + (gramPos - entryPos) + ")|" + word;
						featIndex = getFeatIndex(featStr);
						curEntry.getFtIndexSet().add(featIndex);
					}
				}
			}

		}

		return lEntry;
	}

	/**
	 * Normalize the sentence and add BOS to start and end of sentence,
	 * 
	 * @param sentence
	 * @return
	 */
	protected String prepareSentence(String sentence) {
		sentence = TextProcessing.normalize(sentence);
		for (int i = 0; i < Global.WINDOW_LEN; i++) {
			sentence = "BOS " + sentence + " BOS";
		}
		return sentence;
	}

}
