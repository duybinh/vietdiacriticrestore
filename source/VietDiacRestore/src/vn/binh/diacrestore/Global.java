package vn.binh.diacrestore;

public class Global {
	public enum RunMode {
		Learn, Predict
	}

	// constants
	public static final int DEFAULT_LABEL = 1;
	public static final int WINDOW_LEN = 3;
	public static final int MAX_WORD_LEN = 3;

	public static WordStore wordStore = null;
	public static SylStore sylStore = null;

}
