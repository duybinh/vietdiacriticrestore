package vn.binh.diacrestore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;

/**
 * Learning using SVM method provided by liblinear
 * 
 * @author binhnd
 * 
 */
public class Learner extends Machine {
	/**
	 * Key: Label Value: List of feature's indexes
	 */
	private List<FeatEntry> trainData;

	private Parameter parameter;
	private String corpusPath;

	public Learner(String _corpusPath, String _modelPath, String _featPath) {
		super(_modelPath, _featPath);

		corpusPath = _corpusPath;

		// initial properties
		trainData = new ArrayList<FeatEntry>();

		SolverType solver = SolverType.L1R_LR; // -s 0
		double C = 1.0; // cost of constraints violation
		double eps = 0.01; // stopping criteria
		parameter = new Parameter(solver, C, eps);

	}

	/**
	 * Run training using the corpus
	 * 
	 * @param selectFeat
	 *            true - remove unused features, false - keep original
	 */
	public void train(boolean selectFeat) {
		try {
			extractCorpus();
			learn();
			if (selectFeat) {
				featSelect();
			} else {
				saveResult();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void extractCorpus() throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(corpusPath));
		String line;

		int count = 0;
		List<FeatEntry> lEntry;
		while ((line = reader.readLine()) != null) {
			count++;
			lEntry = extractFeat(line);
			trainData.addAll(lEntry);
			Logger.log("Extract line: " + count + " entry count " + lEntry.size());
		}
		reader.close();

		Logger.log("Training total entry: " + trainData.size());
	}

	private void learn() {
		// get problem and training
		Problem prob = new Problem();
		prob.bias = -1;
		prob.l = trainData.size();
		prob.n = featStore.size();

		// matrix of feature in training data
		prob.x = new Feature[prob.l][];
		// list of target label in training data
		prob.y = new double[prob.l];

		int row = 0;
		for (FeatEntry entry : trainData) {
			// set label value
			prob.y[row] = entry.getLabel();
			// fill feature values
			prob.x[row] = entry.getFtArr();
			row++;
		}

		// training
		model = Linear.train(prob, parameter);
	}

	private void saveResult() throws IOException {
		// save model to file
		File modelFile = new File(modelPath);
		model.save(modelFile);
		featStore.save();
	}

	// remove unused features and save data to file
	private void featSelect() {
		// find unused features
		Set<Integer> unused = new HashSet<>();
		int numFeat = model.getNrFeature();
		int numLabel = model.getNrClass();
		double[] w = model.getFeatureWeights();
		if (w.length != numFeat * numLabel) {
			Logger.log("Array of feature weight not valid");
			return;
		}

		for (int i = 0; i < numFeat; i++) {
			boolean ignore = true;
			for (int j = 0; j < numLabel; j++) {
				if (!isZero(w[i * numLabel + j])) {
					ignore = false;
				}
			}
			if (ignore) {
				unused.add(i + 1);
			}
		}

		// remove unused features in featStore
		featStore.removeFeats(unused);
		featStore.save();

		// write model to file
		try {
			saveSelectedModel(unused);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveSelectedModel(Set<Integer> unused) throws IOException {
		int numFeat = model.getNrFeature();
		int numLabel = model.getNrClass();
		double[] weights = model.getFeatureWeights();
		int[] labels = model.getLabels();

		BufferedWriter modelOutput = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(modelPath), Charset.forName("ISO-8859-1")));
		Formatter formatter = new Formatter(modelOutput, Locale.ENGLISH);

		try {
			printf(formatter, "solver_type %s\n", parameter.getSolverType().name());
			printf(formatter, "nr_class %d\n", numLabel);

			printf(formatter, "label");
			for (int i = 0; i < numLabel; i++) {
				printf(formatter, " %d", labels[i]);
			}
			printf(formatter, "\n");

			printf(formatter, "nr_feature %d\n", featStore.size());
			printf(formatter, "bias %.16g\n", model.getBias());

			printf(formatter, "w\n");
			for (int i = 0; i < numFeat; i++) {
				if (unused.contains(i + 1))
					continue;

				for (int j = 0; j < numLabel; j++) {
					double value = weights[i * numLabel + j];
					if (value == 0.0) {
						printf(formatter, "%d ", 0);
					} else {
						printf(formatter, "%.16g ", value);
					}
				}
				printf(formatter, "\n");
			}

			formatter.flush();
			IOException ioException = formatter.ioException();
			if (ioException != null)
				throw ioException;
		} finally {
			formatter.close();
		}
	}

	private void printf(Formatter formatter, String format, Object... args) throws IOException {
		formatter.format(format, args);
		IOException ioException = formatter.ioException();
		if (ioException != null)
			throw ioException;
	}

	private boolean isZero(double x) {
		if (x < 1e-10 && x > -1e-10)
			return true;
		return false;
	}

	@Override
	protected List<FeatEntry> extractSyl(String sentence) {
		// normalize before processing
		sentence = prepareSentence(sentence);

		List<FeatEntry> l = new ArrayList<>();
		String[] sylArr = sentence.split(" ");
		for (String syl : sylArr) {
			if (syl == null || syl.equals(" "))
				continue;

			String noDiacSyl = TextProcessing.removeDiac(syl);
			FeatEntry o = new FeatEntry();
			o.setSyl(syl);
			o.setSylNoDiac(noDiacSyl);
			o.setType(Global.sylStore.getType(noDiacSyl));
			o.setLabel(Global.sylStore.getLabel(syl));

			l.add(o);
		}
		return l;
	}

	@Override
	protected int getFeatIndex(String feat) {
		return featStore.insertFeat(feat);
	}

}
