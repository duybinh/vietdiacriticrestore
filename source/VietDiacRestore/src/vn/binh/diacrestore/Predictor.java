package vn.binh.diacrestore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import vn.binh.diacrestore.SylStore.SylType;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;

/**
 * Load trained model from file and provide method prediction to restore
 * diactitic of syllables
 * 
 * @author binhnd
 * 
 */
public class Predictor extends Machine {

	public Predictor(String _modelPath, String _featPath) {
		super(_modelPath, _featPath);

		// load model from file
		try {
			File modelFile = new File(modelPath);
			model = Model.load(modelFile);
			featStore.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Restore diacritic for all syllables of the input sentence
	 * 
	 * @param sentence
	 * @return
	 */
	public String restoreDiac(String sentence) {
		List<FeatEntry> lEntry = extractFeat(sentence);
		String result = "";
		FeatEntry entry;
		for (int i = Global.WINDOW_LEN; i < lEntry.size() - Global.WINDOW_LEN; i++) {
			entry = lEntry.get(i);
			if (entry.getType() == SylType.O || entry.getType() == SylType.N) {
				result += entry.getSylNoDiac() + " ";
			} else {
				double prediction = Linear.predict(model, entry.getFtArr());
				String restoredSyl = Global.sylStore.getSyl(entry.getSylNoDiac(), (int) prediction);
				result += restoredSyl + " ";
			}
		}
		return result.trim();
	}

	/**
	 * Restore diacritic for all syllables in the input file
	 * 
	 * @param inputFile
	 *            path of the input file
	 * @param outputFile
	 *            path of the output file
	 * @return
	 */
	public boolean restoreDiac(String inputFile, String outputFile) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			FileWriter writer = new FileWriter(outputFile);

			String line;
			String restoredLine;
			while ((line = reader.readLine()) != null) {
				restoredLine = restoreDiac(line);
				writer.write(restoredLine + "\n");
			}
			reader.close();

			writer.flush();
			writer.close();

			Logger.log("Restore diacritic success to file " + outputFile);

		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	protected List<FeatEntry> extractSyl(String sentence) {
		// normalize before processing
		sentence = prepareSentence(sentence);

		List<FeatEntry> l = new ArrayList<>();
		String[] sylArr = sentence.split(" ");
		for (String noDiacSyl : sylArr) {
			if (noDiacSyl == null || noDiacSyl.equals(" "))
				continue;
			noDiacSyl = TextProcessing.removeDiac(noDiacSyl);

			FeatEntry o = new FeatEntry();
			o.setSylNoDiac(noDiacSyl);
			o.setType(Global.sylStore.getType(noDiacSyl));

			l.add(o);
		}
		return l;
	}

	@Override
	protected int getFeatIndex(String feat) {
		return featStore.getFeatIndex(feat);
	}
}
