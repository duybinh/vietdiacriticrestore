package vn.binh.diacrestore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Store words in vietnamese dictionary
 * 
 * @author binhnd
 * 
 */
public class WordStore {
	private String filePath;
	private Set<String> wordSet;
	private Set<String> noDiacWordSet;

	public WordStore(String _filePath) {
		filePath = _filePath;
		wordSet = new HashSet<>();
		noDiacWordSet = new HashSet<String>();

		load();
	}

	public boolean isWord(String input, boolean ignoreDiac) {
		input = input.toLowerCase();
		if (ignoreDiac) {
			return noDiacWordSet.contains(input);
		}
		return wordSet.contains(input);
	}

	private void load() {
		try {
			BufferedReader bufReader = new BufferedReader(new FileReader(filePath));
			String line;
			while ((line = bufReader.readLine()) != null) {
				line = line.toLowerCase().trim();

				wordSet.add(line);
				noDiacWordSet.add(TextProcessing.removeDiac(line));
			}
			bufReader.close();

			Logger.log("Loaded total words: " + wordSet.size());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
