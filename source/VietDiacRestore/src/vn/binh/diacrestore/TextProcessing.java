package vn.binh.diacrestore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer;

/**
 * Utility methods for text processing
 * 
 * @author binhnd
 * 
 */
public class TextProcessing {
	public static String removeDiac(String input) {
		input = Normalizer.normalize(input, Normalizer.Form.NFD);
		input = input.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "").replaceAll("đ", "d").replaceAll("Đ", "D");
		return input;
	}

	public static boolean removeDiacFile(String inputFile, String outputFile) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			FileWriter writer = new FileWriter(outputFile);

			String line;
			String rmDiacLine;
			while ((line = reader.readLine()) != null) {
				rmDiacLine = removeDiac(line);
				writer.write(rmDiacLine + "\n");
			}
			reader.close();

			writer.flush();
			writer.close();

			Logger.log("Remove diacritic success to file " + outputFile);

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean isSymbol(char c) {
		return (c != ' ') && !Character.isLetter(c) && !Character.isDigit(c);
	}

	public static String normalize(String input) {
		String output = "";
		input = input + " ";
		char c1, c2;
		for (int i = 0; i < input.length() - 1; i++) {
			c1 = input.charAt(i);
			c2 = input.charAt(i + 1);
			if (c1 == ' ' && c2 == ' ')
				continue;

			if ((Character.isLetter(c1) && isSymbol(c2)) || (isSymbol(c1) && Character.isLetter(c2))) {
				output += c1 + " ";
				continue;
			}

			output += c1;
		}

		return output.trim();
	}

	public static boolean normalizeFile(String inputFile, String outputFile) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			FileWriter writer = new FileWriter(outputFile);

			String line;
			String outLine;
			while ((line = reader.readLine()) != null) {
				outLine = normalize(line);
				if (isValidSentence(outLine))
					writer.write(outLine + "\n");
			}
			reader.close();

			writer.flush();
			writer.close();

			Logger.log("Normalize text success to file " + outputFile);

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private static String[] invalidDiacArr = { " ̉ ", " ̀ ", " ́ ", " ̣ ", " ̃ " };

	public static boolean isValidSentence(String input) {
		for (String invalidDiac : invalidDiacArr) {
			if (input.contains(invalidDiac))
				return false;
		}

		String nonDiacSentence = removeDiac(input);
		if (nonDiacSentence.equals(input))
			return false;

		String[] syls = input.split(" ");
		double invalidSyl = 0.0;
		double totalSyl = 0.0;
		boolean isValidSyl = false;
		for (String syl : syls) {
			if (syl == null || syl.trim() == "")
				continue;

			if (!Global.sylStore.isSyl(syl, false)) {
				isValidSyl = false;
				for (int i = 0; i < syl.length(); i++) {
					if (!Character.isLetter(syl.charAt(i))) {
						isValidSyl = true;
					}
				}
				if (!isValidSyl)
					invalidSyl++;
			}
			totalSyl++;
		}
		if (totalSyl == 0)
			return false;

		return invalidSyl / totalSyl < 0.2;
	}

	public static boolean splitFile(String inputFile, String outputFile, int fromLine, int lineCount) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			FileWriter writer = new FileWriter(outputFile);

			String line;
			int index = 0;
			while ((line = reader.readLine()) != null) {
				index++;

				if (index < fromLine)
					continue;

				if (lineCount > 0 && index - fromLine >= lineCount)
					break;

				writer.write(line + "\n");
			}
			reader.close();

			writer.flush();
			writer.close();

			Logger.log("Copy " + lineCount + " success to file " + outputFile);

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static String correctCase(String noDiacSyl, String syl) {
		if (noDiacSyl.length() != syl.length())
			return syl;

		String output = "";
		for (int i = 0; i < noDiacSyl.length(); i++) {
			if (Character.isUpperCase(noDiacSyl.charAt(i))) {
				output += Character.toUpperCase(syl.charAt(i));
			} else {
				output += syl.charAt(i);
			}
		}
		return output;
	}
}
