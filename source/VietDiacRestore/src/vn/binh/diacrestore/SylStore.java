package vn.binh.diacrestore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Store Vietnamese syllables
 * 
 * @author binhnd
 * 
 */
public class SylStore {
	private String filePath;

	// set of syllables (with diacritic)
	private Set<String> sylSet;

	/*
	 * key is syllable without diacritic, value is set of possible original
	 * syllables
	 */
	private Map<String, Set<String>> noDiacSylMap;

	public SylStore(String _filePath) {
		filePath = _filePath;
		sylSet = new HashSet<>();
		noDiacSylMap = new HashMap<>();

		load();
	}

	/**
	 * check whether input word is vietnamese syllable
	 * 
	 * @param input
	 * @param ignoreDiac
	 *            don't care about diacritic
	 * @return
	 */
	public boolean isSyl(String input, boolean ignoreDiac) {
		input = input.toLowerCase();
		if (ignoreDiac) {
			return noDiacSylMap.containsKey(input);
		}
		return sylSet.contains(input);
	}

	/**
	 * Get label of a syllable
	 * 
	 * @param syl
	 * @return label if the index of syllable in the set possible syllable in
	 *         noDiacSylMap
	 */
	public int getLabel(String syl) {
		syl = syl.toLowerCase();
		String noDiacSyl = TextProcessing.removeDiac(syl);

		if (noDiacSyl.equals(syl))
			return Global.DEFAULT_LABEL;

		Set<String> set = noDiacSylMap.get(noDiacSyl);
		if (set == null)
			return Global.DEFAULT_LABEL;

		int index = 0;
		for (String s : set) {
			if (s.equals(syl))
				return index + 2;
			index++;
		}

		return Global.DEFAULT_LABEL;
	}

	/**
	 * Get syllable with diacritic
	 * 
	 * @param noDiacSyl
	 *            syllable without diacritic
	 * @param label
	 *            predict label
	 * @return
	 */
	public String getSyl(String noDiacSyl, int label) {

		if (label <= 1)
			return noDiacSyl;

		String lNoDiacSyl = noDiacSyl.toLowerCase();
		Set<String> set = noDiacSylMap.get(lNoDiacSyl);
		if (set == null)
			return noDiacSyl;

		if (label > set.size() + 1)
			return noDiacSyl;

		for (String syl : set) {
			if (label-- == 2) {
				return TextProcessing.correctCase(noDiacSyl, syl);
			}
		}
		return noDiacSyl;
	}

	public final String SYMBOLS = "@`#$%&~|[]<>'(){}*+-=;,?.!:\"/";

	public enum SylType {
		O, N, L, U
	}

	/**
	 * Get type of syllable
	 * 
	 * @param syl
	 * @return O : other N : number U : Upper L : lower
	 */
	public SylType getType(String syl) {
		boolean u = false;
		boolean l = false;
		boolean n = false;
		boolean o = false;

		for (int i = 0; i < syl.length(); ++i) {
			char c = syl.charAt(i);
			if (Character.isDigit(c)) {
				n = true;
			} else if (Character.isUpperCase(c)) {
				u = true;
			} else if (Character.isLowerCase(c)) {
				l = true;
			} else {
				o = true;
			}
		}

		if (o)
			return SylType.O;

		if (n && (!(u || l)))
			return SylType.N;

		if (n && (u || l))
			return SylType.O;

		if (isSyl(syl, true)) {
			if (u)
				return SylType.U;
			if (l)
				return SylType.L;
		}

		return SylType.O;
	}

	private void load() {
		try {
			BufferedReader bufReader = new BufferedReader(new FileReader(filePath));

			String line;
			while ((line = bufReader.readLine()) != null) {
				line = line.toLowerCase().trim();

				sylSet.add(line);

				String noDiacSyl = TextProcessing.removeDiac(line);
				Set<String> s = noDiacSylMap.get(noDiacSyl);
				if (s == null) {
					s = new TreeSet<String>();
					noDiacSylMap.put(noDiacSyl, s);
				}
				s.add(line);
			}

			bufReader.close();

			Logger.log("Loaded total syl: " + sylSet.size() + "/" + noDiacSylMap.size());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
